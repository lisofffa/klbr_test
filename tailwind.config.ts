import type { Config } from 'tailwindcss';

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  darkMode: 'media',
  theme: {
    extend: {
      screens: {
        sm: '820px',
      },
      colors: {
        'dpl-white': '#e7ecef',
      },
      spacing: {
        '128': '32rem',
      },
      borderRadius: {
        '6xl': '10px',
        '50xl': '50%',
      },
      width: {
        '277': '277px',
      },
      gap: {
        '40': '35px',
      },
      maxWidth: {
        mwLg: '748px',
      },
      order: {
        '0': '0',
      },
    },
  },
  plugins: [],
};
export default config;
