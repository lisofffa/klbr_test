export const generateRandomWord = (length: number) => {
  const characters = 'abcdefghijklmnopqrstuvwxyz';
  let randomWord = '';

  for (let i = 0; i < length; i++) {
    const randomIndex = Math.floor(Math.random() * characters.length);
    randomWord += characters.charAt(randomIndex);
  }
  const result = randomWord.charAt(0).toUpperCase() + randomWord.slice(1);
  return result;
};
