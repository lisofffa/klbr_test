import { generateRandomWord } from './generateRandomWord';

export let arrayOfObjects = Array.from({ length: 100 }, (_, index) => ({
  key: index,
  name: generateRandomWord(4),
  surname: generateRandomWord(3),
}));
