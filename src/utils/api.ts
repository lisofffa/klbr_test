import { arrayOfObjects } from './generateArray';
export const fetchData = async () => {
  return new Promise<Array<{ name: string; surname: string }>>((resolve) => {
    setTimeout(() => {
      resolve(arrayOfObjects);
    }, 2500);
  });
};
