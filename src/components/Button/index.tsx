import cn from "classnames"
export interface ButtonProps {
  text: string;
  onClick: () => void;
  disabled: boolean;
}

export function Button({ text, onClick, disabled }: ButtonProps) {
  return (
    <button
      disabled={disabled}
      onClick={onClick}
      className={cn("mt-8 px-4 py-2 rounded-full", {
        'bg-sky-500 hover:bg-sky-700 text-white': !disabled,
        'bg-gray-300 cursor-not-allowed': disabled,
      })}
    >
      {text}
    </button>
  );
}
