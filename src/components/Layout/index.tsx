import { FC, PropsWithChildren, ReactNode } from 'react';

import Head from 'next/head';

interface LayoutProps extends PropsWithChildren {
  title?: string | ReactNode;
  setTitle?: any;
  description?: string;
}

export const Layout: FC<LayoutProps> = ({ children, title, description }) => {
  return (
    <>
      <Head>
        <link rel='manifest' href='/manifest.json'></link>
        <meta name='theme-color' content='#e7ecef' />
        <title>{title}</title>
        <meta name='description' content={description} />
      </Head>
      <main className='flex min-h-screen flex-col items-center justify-center bg-white p-8 dark:bg-black'>
        {children}
      </main>
    </>
  );
};
