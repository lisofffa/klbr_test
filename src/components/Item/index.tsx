import styles from './styles.module.scss';
import { dataObject } from '@/types/data';

export interface ItemProps {
  item: dataObject;
}

export function Item({ item }: ItemProps) {
  return (
    <li className={`flex items-center ${styles['list-item']}`}>
      <span className='mr-2 text-gray-700 dark:text-gray-300'>{item.name}</span>
      <span className='mr-2 text-gray-700 dark:text-gray-300'>
        {item.surname}
      </span>
    </li>
  );
}
