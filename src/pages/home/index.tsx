import { Button } from '@/components/Button';
import { Layout } from '@/components/Layout';
import { dataObject } from '@/types/data';
import { useEffect, useState } from 'react';
import styles from './styles.module.scss';
import { fetchData } from '@/utils/api';
import { Item } from '@/components/Item';

function Home() {
  const [items, setItems] = useState<dataObject[] | null>(null);
  const [visibleItems, setVisibleItems] = useState(20);
  const [isButtonDisabled, setIsButtonDisabled] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const fetchDataAndUpdateState = async () => {
      try {
        const newData = await fetchData();
        setItems(newData);
        setIsLoading(false);
      } catch (error) {
        console.error('Error fetching data:', error);
        setIsLoading(false);
      }
    };

    fetchDataAndUpdateState();
  }, []);

  useEffect(() => {
    setIsButtonDisabled(visibleItems >= (items?.length || 0));
  }, [visibleItems, items]);

  const showMoreItems = () => {
    setVisibleItems((prevVisibleItems) => prevVisibleItems + 20);
  };

  return (
    <Layout title='...' description='...'>
      {isLoading ? (
        <div className={`${styles.loader} mt-8`} />
      ) : (
        <>
          <ul className='mt-4 flex flex-col items-center justify-center space-y-2'>
            {items &&
              items
                .slice(0, visibleItems)
                .map((item, index) => <Item item={item} key={index} />)}
          </ul>

          <Button
            disabled={isButtonDisabled}
            onClick={showMoreItems}
            text='More'
          />
        </>
      )}
    </Layout>
  );
}

export default Home;
