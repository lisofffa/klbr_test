import type { AppProps } from 'next/app';
import { FC, useEffect } from 'react';
import { ThemeProvider } from 'next-themes';
import init from '@socialgouv/matomo-next';

import '@/assets/styles/globals.css';

const MyApp: FC<AppProps> = ({ Component, ...rest }) => {

  return (
    <ThemeProvider
      attribute='class'
      defaultTheme='system'
      disableTransitionOnChange
    >
      <Component {...rest} />
    </ThemeProvider>
  );
};

export default MyApp;
